package com.cashmanager.api;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cashmanager.model.Cart;
import com.cashmanager.model.Product;

@RequestMapping("api/cart")
public class CartController {
	
	private Map<UUID, Cart> carts;
	
	public CartController() {
		this.carts = new HashMap<UUID, Cart>();
	}
	
	@PostMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public boolean addProductToCart(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody Product product) {
       
		if(this.carts.containsKey(id)){
			Cart userCart = this.carts.get(id);
				if(userCart.getUserCart().containsKey(product)) {
					int nbOfProduct = userCart.getUserCart().get(product);
					nbOfProduct++;
					userCart.getUserCart().put(product, nbOfProduct);
					this.carts.put(id, userCart);
					return true;
				}
		}
		
		Cart userCart = new Cart();
		userCart.getUserCart().put(product, 1);
		
		this.carts.put(id, userCart);
		return true;
		
    }
	
	@GetMapping(path = "getUserCart/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Map<Product, Integer> getUserCart(@NotNull @PathVariable("id") UUID id) {
        
		if(this.carts.containsKey(id)) {
			Cart userCart = this.carts.get(id);
			return userCart.getUserCart();
		}
		
		return null;
		
    }
	
	@DeleteMapping(path = "removeOne/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public boolean removeOneProductFromCart(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody Product product) {
		
		if(this.carts.containsKey(id)){
			Cart userCart = this.carts.get(id);
				if(userCart.getUserCart().containsKey(product)) {
					int nbOfProduct = userCart.getUserCart().get(product);
					if(nbOfProduct > 1) {
						nbOfProduct--;
						userCart.getUserCart().put(product, nbOfProduct);
						this.carts.put(id, userCart);
						return true;
					}
					else {
						userCart.getUserCart().remove(product);
						this.carts.put(id, userCart);
						return true;
					}
				}
		}
		return false;
	}
	
	//delete a product from cart
	@DeleteMapping(path = "deleteProduct/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public boolean deleteProductFromCart(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody Product product) {
		if(this.carts.containsKey(id)) {
			Cart userCart = this.carts.get(id);
			if(userCart.getUserCart().containsKey(product)) {
				userCart.getUserCart().remove(product);
				this.carts.put(id, userCart);
				return true;
			}
			return false;
		}
		return false;
	}
	
	//get number of product
	@GetMapping(path = "getNumberOfProductInCart/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public int getNumberOfProductInCart(@PathVariable("id") UUID id) {
		
		int nbOfProducts = 0;
		
		if(this.carts.containsKey(id)) {
			Cart userCart = this.carts.get(id);
			for(Map.Entry<Product, Integer> product : userCart.getUserCart().entrySet()) {
			    nbOfProducts += product.getValue();
			}
		}
		
		return nbOfProducts;
		
	}
	
	//get total of cart
	@GetMapping(path = "getCartTotalPrize/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public float getCartTotalPrize(@PathVariable("id") UUID id) {
		
		float total = 0;
		
		if(this.carts.containsKey(id)) {
			Cart userCart = this.carts.get(id);
			for(Map.Entry<Product, Integer> product : userCart.getUserCart().entrySet()) {
			    total += product.getKey().getPrice() *  product.getValue();
			}
		}
		
		return total;
	}
	
	//get total without taxes of the cart
	@GetMapping(path = "getCartTotalPrizeWithoutTaxes/{id}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public float getCartTotalPriceWithoutTaxes(@PathVariable("id") UUID id) {
		
		float total = 0;
		
		if(this.carts.containsKey(id)) {
			Cart userCart = this.carts.get(id);
			for(Map.Entry<Product, Integer> product : userCart.getUserCart().entrySet()) {
			    total += product.getKey().getPrice_wt() *  product.getValue();
			}
		}
		
		return total;
	}
	
}
