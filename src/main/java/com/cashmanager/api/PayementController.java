package com.cashmanager.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cashmanager.model.Payement;

@RequestMapping("payement")
@RestController
public class PayementController {
	
	private Payement payement;
	
	public PayementController() {
		this.payement = new Payement();
	}
	
	@PostMapping
    public boolean doPayement(boolean bool){
       return this.payement.doPayement();
    }

}
