package com.cashmanager.api;

import com.cashmanager.model.Order;
import com.cashmanager.model.User;
import com.cashmanager.service.PaymentService;
import com.cashmanager.service.PaymentServiceImpl;
import com.cashmanager.service.StripeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/api/payment")
@RestController
@CrossOrigin
public class PaymentController {
    @Autowired
    PaymentServiceImpl paymentService;
/*    // Reading the value from the application.properties file
    @Value("${STRIPE_PUBLIC_KEY}")
    private String stripePublicKey;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping()
    public String home(Model model) {
        model.addAttribute("amount", 50 * 100); // In cents
        model.addAttribute("stripePublicKey", stripePublicKey);
        return "index";
    }*/

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @PostMapping()
    public String chargeCard() throws Exception {
        Order order = new Order();
        order.setAmount((long) 100);
        User user = new User();

        user.setEmail("amir@company.com");
        user.setName("Amir");

// Create Stripe customer
        String id = paymentService.createCustomer(user);

// Assign ID to user
        user.setStripeCustomerId(id);
        order.setUser(user);
        paymentService.chargeCreditCard(order);
        return "result";
    }
}
