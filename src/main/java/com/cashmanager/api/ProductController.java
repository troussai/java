package com.cashmanager.api;

import com.cashmanager.model.Product;
import com.cashmanager.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("api/products")
@RestController
@CrossOrigin
public class ProductController {
    private final ProductServiceImpl productService;

    @Autowired
    public ProductController(ProductServiceImpl productServiceImpl) {
        this.productService = productServiceImpl;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public void addProduct(@Valid @RequestBody Product product) {
        productService.addProduct(product);
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Product getProductById(@PathVariable("id")UUID id) {
        return productService.getProductById(id).orElse(null);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteProductById(@PathVariable("id")UUID id) {
        productService.deleteProduct(id);
    }
    @PutMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> updateProduct (@PathVariable("id") UUID id, @Valid @RequestBody Product productToUpdate) {
        if (productService.getProductById(id).isPresent())
        {
            productService.updateProduct(id, productToUpdate);
        }
        return ResponseEntity.ok("Product updated");
    }
}
