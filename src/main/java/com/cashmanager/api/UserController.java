package com.cashmanager.api;

import com.cashmanager.model.User;
import com.cashmanager.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequestMapping("api/users")
@RestController
@CrossOrigin
public class UserController {
    private final UserServiceImpl userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

   /* @GetMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Optional<User> getUserById(@PathVariable("id") UUID id) {
        return userService.getUserById(id);
    }*/

    @GetMapping(path = "{username}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Optional<User> getUserByUsername(@PathVariable("username") String username) {
        return userService.getUserByUsername(username);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public void deleteUserById(@PathVariable("id") UUID id) {
        userService.deleteUser(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<Object> updateUser(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody User personToUpdate) {
        if (userService.getUserById(id).isPresent())
        {
            userService.updateUser(id, personToUpdate);
        }
        return ResponseEntity.ok("User updated");
    }
}
