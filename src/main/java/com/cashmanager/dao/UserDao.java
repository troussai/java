package com.cashmanager.dao;

import com.cashmanager.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserDao {
    List<User> selectAllUser();

    Optional<User> selectUserById(UUID id);

    int deleteUserById(UUID id);
    int updateUserById(UUID id, User person);
    int insertUser (UUID id, User person);

	String authenticateUser(String userName, String password);
}
