package com.cashmanager.dao;

import com.cashmanager.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class UserDataAccessService implements UserDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertUser(UUID id, User person) {
        return 0;
    }

    @Override
    public List<User> selectAllUser() {
        final String sql = "SELECT id, userName, email, password FROM users";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String userName = resultSet.getString("userName");
            String email = resultSet.getString("email");
            String password1 = resultSet.getString("password");
            return new User(id, userName, email, password1);
        });
    }

    @Override
    public Optional<User> selectUserById(UUID id) {
        return Optional.empty();
    }

    @Override
    public int deleteUserById(UUID id) {
        return 0;
    }

    @Override
    public int updateUserById(UUID id, User person) {
        return 0;
    }
    
    @Override
    public String authenticateUser(String userName, String password){
    	final String sql = "SELECT id FROM users WHERE username = '" + userName +"'" + " AND password = '" + password + "'";
    	
    	String userId = (String) jdbcTemplate.queryForObject(sql, String.class);
    	
    	return userId;
    }
    
}
