package com.cashmanager.model;

import java.util.HashMap;
import java.util.Map;

public class Cart {
	
	//List of products of the cart
	private Map<Product, Integer> listOfProducts;
	
	public Cart() {
		this.listOfProducts = new HashMap<Product, Integer>();
	}
	
	//getters and setters
	public Map<Product, Integer> getUserCart(){
		return this.listOfProducts;
	}
	
	public void setUserCart(Map<Product, Integer> userCart) {
		this.listOfProducts = userCart;
	}
	
}
