package com.cashmanager.model;

import java.math.BigDecimal;

public class Order {
    private Long amount;
    private User user;


    public Order(Long amount, User user) {
        this.amount = amount;
        this.user = user;
    }

    public Order() {}

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
