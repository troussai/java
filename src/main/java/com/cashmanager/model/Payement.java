package com.cashmanager.model;

public class Payement {
	
	private int payementAttempts;
	
	private boolean blockPayement;
	
	public Payement() {
		this.payementAttempts = 0;
		this.blockPayement = false;
	}
	
	public boolean doPayement() {
		if(this.payementAttempts < 3 && !this.blockPayement) {
			this.payementAttempts ++;
		}else {
			this.payementIsBlocked();
			return false;
		}
		return true;
	}
	
	public void increasePayementAttemps() {
		this.payementAttempts ++;
	}
	
	public void payementIsSuccesfull() {
	}
	
	public void payementIsBlocked() {
		this.blockPayement = true;
	}

}
