package com.cashmanager.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.*;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @NotBlank
    private String path_image;

    @NotBlank
    private String name;

    @NotBlank
    private Float price;

    @NotBlank
    private Float price_wt;

    public Product(String name, String path_image, Float price, Float price_wt) {
        this.name = name;
        this.path_image = path_image;
        this.price = price;
        this.price_wt = price_wt;
    }

    protected Product() {}


    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice_wt() {
        return price_wt;
    }

    public void setPrice_wt(Float price_wt) {
        this.price_wt = price_wt;
    }
}
