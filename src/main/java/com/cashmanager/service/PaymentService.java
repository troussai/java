package com.cashmanager.service;

import com.cashmanager.model.Order;
import com.cashmanager.model.User;

public interface PaymentService {
    public String createCustomer(User user);
    public void chargeCreditCard(Order order);
}
