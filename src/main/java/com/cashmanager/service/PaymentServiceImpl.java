package com.cashmanager.service;

import com.cashmanager.model.Order;
import com.cashmanager.model.User;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Value("sk_test_DUP0xNPQVqQQe8dKxz863XHB00TkOP05qu")
    private String API_SECRET_KEY;

    public PaymentServiceImpl() {
        Stripe.apiKey = "sk_test_DUP0xNPQVqQQe8dKxz863XHB00TkOP05qu";//API_SECRET_KEY;
    }
    @Override
    public String createCustomer(User user) {
        Map<String, Object> customerParams = new HashMap<String, Object>();
        customerParams.put("description", user.getName());
        customerParams.put("email", user.getEmail());

        String id = null;

        try {
            Customer stripeCustomer = Customer.create(customerParams);
            id = stripeCustomer.getId();
            System.out.println(stripeCustomer);
        }  catch (CardException e) {
            // Since it's a decline, CardException will be caught
            System.out.println("Status is: " + e.getCode());
            System.out.println("Message is: " + e.getMessage());
        } catch (RateLimitException e) {
            // Too many requests made to the API too quickly
        } catch (InvalidRequestException e) {
            // Invalid parameters were supplied to Stripe's API
        } catch (AuthenticationException e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        } catch (StripeException e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
        } catch (Exception e) {
            // Something else happened, completely unrelated to Stripe
        }
        return id;
    }

    @Override
    public void chargeCreditCard(Order order) {
        Long chargeAmount = order.getAmount() * 100;

        User user = order.getUser();

        Map<String, Object> chargeParams = new HashMap<String, Object>();
        chargeParams.put("amount", chargeAmount);
        chargeParams.put("currency", "eur");
        chargeParams.put("description", "");
        //chargeParams.put("customer", user.getStripeCustomerId());

        try {
            Charge charge = Charge.create(chargeParams);
            System.out.println(charge);
        } catch (RateLimitException e) {
            // Too many requests made to the API too quickly
        } catch (InvalidRequestException e) {
            // Invalid parameters were supplied to Stripe's API
        } catch (AuthenticationException e) {
            // Authentication with Stripe's API failed (wrong API key?)
        } catch (StripeException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // Something else happened unrelated to Stripe
        }
    }
}
