package com.cashmanager.service;


import com.cashmanager.model.Product;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductService {
    public abstract Optional<Product> getProductById(UUID id);
    public abstract ResponseEntity<String> addProduct(Product product);
    public abstract void updateProduct(UUID id, Product product);
    public abstract void deleteProduct(UUID id);
    public abstract List<Product> getAllProducts();
}
