package com.cashmanager.service;

import com.cashmanager.model.Product;
import com.cashmanager.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public Optional<Product> getProductById(UUID id) {
        return productRepository.findById(id);
    }

    @Transactional
    public ResponseEntity<String> addProduct(Product product) {
        if (productRepository.existsByName(product.getName())) {
            return new ResponseEntity<String>("Fail -> Product already exist",
                    HttpStatus.BAD_REQUEST);
        }
        Product newProduct = new Product(product.getName(), product.getPath_image(),
                product.getPrice(), product.getPrice_wt());
        productRepository.save(newProduct);
        return ResponseEntity.ok().body("Product created successfully");
    }

    @Transactional
    public void updateProduct(UUID id, Product product) {
        product.setId(id);
        productRepository.save(product);
    }

    @Transactional
    public void deleteProduct(UUID id) {
        productRepository.deleteById(id);
    }

    @Transactional
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
