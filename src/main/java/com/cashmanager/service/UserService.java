package com.cashmanager.service;

import com.cashmanager.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    public Optional<User> getUserByUsername(String username);
    public abstract Optional<User> getUserById(UUID id);
    public abstract void saveUser(User user);
    public abstract void updateUser(UUID id, User user);
    public abstract void deleteUser(UUID id);
    public abstract List<User> getAllUsers();

}
