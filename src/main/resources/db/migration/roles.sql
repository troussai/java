
CREATE TABLE user_roles
(
    user_id uuid NOT NULL,
    role_id uuid NOT NULL,
    CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT fkh8ciramu9cc9q3qcqiv4ue8a6 FOREIGN KEY (role_id)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkhfh9dx7w3ubf1co1vdev94g3f FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
CREATE TABLE roles
(
    id uuid NOT NULL,
    name character varying(60) COLLATE pg_catalog."default",
    CONSTRAINT roles_pkey PRIMARY KEY (id),
    CONSTRAINT uk_nb4h0p6txrmfc0xbrd1kglp9t UNIQUE (name)

);

INSERT INTO roles(id, name) VALUES
('9e9133a3-be0a-4e41-ad77-ec3a76a11893','ROLE_USER'),
('ba98e1a7-3fb7-42ee-98b7-55c49331390f','ROLE_ADMIN');
